<?php
/**
 * Created by PhpStorm.
 * User: Pawel Plocharczyk
 * Date: 18.04.2017
 * Time: 11:02
 */

namespace KrdApi\Source;


use KrdApi\Authorization\AuthorizationInterface;
use KrdApi\Request\RequestInterface;
use KrdApi\Service\ServiceParameters;
use Zend\Soap\Client;

class KrdRemote implements SourceInterface
{

    public function fetch(RequestInterface $request, ServiceParameters $serviceParameters, AuthorizationInterface $authorization)
    {
        $client = new Client($serviceParameters->wsdl->toNative());
        $authorizationHeader = $authorization->getHeader();
        $header = new \SoapHeader($serviceParameters->namespace->toNative(), 'Authorization', $authorizationHeader);
        $client->addSoapInputHeader($header);
        $body = $request->getRequestBody();
        $method = $request->getMethodName();
        $client->setSoapVersion(1);
        $response = $client->$method($body);
        return $response;
    }
}