<?php
/**
 * Created by PhpStorm.
 * User: Pawel Plocharczyk
 * Date: 18.04.2017
 * Time: 11:11
 */

namespace KrdApi\Source;


use KrdApi\Authorization\AuthorizationInterface;
use KrdApi\Request\RequestInterface;
use KrdApi\Service\ServiceParameters;

interface SourceInterface
{
    public function fetch(RequestInterface $request, ServiceParameters $serviceParameters, AuthorizationInterface $authorization);
}