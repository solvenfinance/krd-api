<?php
/**
 * Created by PhpStorm.
 * User: Pawel Plocharczyk
 * Date: 18.04.2017
 * Time: 10:45
 */

namespace KrdApi;

use KrdApi\Authorization\AuthorizationInterface;
use KrdApi\Exception\ServiceNotFoundException;
use KrdApi\Service\ServiceInterface;

class KrdApi
{
    private $authorization;
    private $services;

    public function __construct(AuthorizationInterface $authorization)
    {
        $this->authorization = $authorization;
    }

    public function getService($service): ServiceInterface
    {
        if(isset($this->services[$service])){
            return $this->services[$service];
        }
        throw new ServiceNotFoundException('Service ' . $service . ' not found');
    }

    public function registerService(ServiceInterface $service)
    {
        /** Przemyslec czy auth nie dodac do konstruktora serwisu - (wysylanie  danych od nas nie bedzie korzystac z soap?) */
        $service->setAuthorization($this->authorization);
        $this->services[get_class($service)] = $service;
    }
}