<?php
/**
 * Created by PhpStorm.
 * User: Pawel Plocharczyk
 * Date: 18.04.2017
 * Time: 10:46
 */

namespace KrdApi\Service;


use KrdApi\Authorization\AuthorizationInterface;
use KrdApi\Request\SearchCustomerRequest;
use KrdApi\Source\SourceInterface;

class SearchCustomer implements  ServiceInterface
{
    private $serviceParameters;
    private $source;
    private $authorization;
    public function __construct(ServiceParameters $serviceParameters, SourceInterface $source)
    {
        $this->serviceParameters = $serviceParameters;
        $this->source = $source;
    }

    public function execute(SearchCustomerRequest $request)
    {
        if(!$this->authorization) {
            throw new \DomainException('Authorization data not found');
        }
        $response = $this->source->fetch($request, $this->serviceParameters, $this->authorization);
        /* TODO: mapowanie */
        return $response;
    }

    public function setAuthorization(AuthorizationInterface $authorization)
    {
        $this->authorization = $authorization;
    }
}