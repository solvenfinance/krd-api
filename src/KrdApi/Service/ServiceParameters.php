<?php
/**
 * Created by PhpStorm.
 * User: Pawel Plocharczyk
 * Date: 18.04.2017
 * Time: 11:10
 */

namespace KrdApi\Service;


use KrdApi\Authorization\AuthorizationInterface;
use KrdApi\ValueObject\StringValue;
use StdDomain\Entity\AccessibleEntityTrait;

class ServiceParameters
{
    use AccessibleEntityTrait;
    private $wsdl;
    private $namespace;
    public function __construct(StringValue $wsdl, StringValue $namespace)
    {
        $this->wsdl = $wsdl;
        $this->namespace = $namespace;
    }
}