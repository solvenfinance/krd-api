<?php
/**
 * Created by PhpStorm.
 * User: Pawel Plocharczyk
 * Date: 18.04.2017
 * Time: 10:50
 */

namespace KrdApi\Request;


use KrdApi\ValueObject\DateTime;
use KrdApi\ValueObject\NumberType;
use KrdApi\ValueObject\StringValue;
use StdDomain\Entity\AccessibleEntityTrait;

class SearchCustomerRequest implements RequestInterface
{
    use AccessibleEntityTrait;

    /**
     * @var NumberType
     */
    private $type;

    /**
     * @var StringValue
     */
    private $value;

    /**
     * @var DateTime
     */
    private $date;

    /**
     * SearchCustomerRequest constructor.
     * @param NumberType $type
     * @param StringValue $value
     * @param DateTime $date
     */
    public function __construct(NumberType $type, StringValue $value, DateTime $date)
    {
        $this->type = $type;
        $this->value = $value;
        $this->date = $date;
    }

    public function getRequestBody()
    {
        return [
            'Number' => $this->value->toNative(),
            'NumberType' => $this->type->getValue(),
            'AuthorizationDate' => $this->date->toNative()
        ];
    }

    public function getMethodName()
    {
        return 'SearchConsumer';
    }
}