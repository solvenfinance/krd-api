<?php
/**
 * Created by PhpStorm.
 * User: Pawel Plocharczyk
 * Date: 18.04.2017
 * Time: 10:55
 */

namespace KrdApi\Request;


interface RequestInterface
{
    public function getRequestBody();
    public function getMethodName();
}