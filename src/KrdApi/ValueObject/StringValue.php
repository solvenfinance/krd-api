<?php
/**
 * Created by PhpStorm.
 * User: Pawel Plocharczyk
 * Date: 18.04.2017
 * Time: 10:52
 */

namespace KrdApi\ValueObject;


use StdDomain\ValueObject\InvalidNativeArgumentException;
use StdDomain\ValueObject\ValueObjectInterface;
use StdDomain\ValueObject\ValueObjectTrait;

class StringValue implements ValueObjectInterface
{
    const NOT_STRING = 'notString';

    use ValueObjectTrait;

    public function __construct($value)
    {
        if (!is_string($value)) {
            throw new InvalidNativeArgumentException("Value isn't a string", self::NOT_STRING);
        }

        $this->value = $value;
    }
}