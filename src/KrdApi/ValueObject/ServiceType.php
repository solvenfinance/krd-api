<?php
/**
 * Created by PhpStorm.
 * User: Pawel Plocharczyk
 * Date: 18.04.2017
 * Time: 10:57
 */

namespace KrdApi\ValueObject;

use StdDomain\ValueObject\Enum;

class ServiceType extends Enum
{
    const SearchCustomer = 'searchCustomer';
}