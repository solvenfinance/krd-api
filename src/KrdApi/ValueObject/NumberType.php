<?php
/**
 * Created by PhpStorm.
 * User: Pawel Plocharczyk
 * Date: 18.04.2017
 * Time: 10:47
 */

namespace KrdApi\ValueObject;


use MabeEnum\Enum;

class NumberType extends Enum
{
    const PESEL = 'Pesel';
}