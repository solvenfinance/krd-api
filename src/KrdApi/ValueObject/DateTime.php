<?php
/**
 * Created by PhpStorm.
 * User: Pawel Plocharczyk
 * Date: 18.04.2017
 * Time: 10:51
 */

namespace KrdApi\ValueObject;


use StdDomain\ValueObject\ValueObjectInterface;

class DateTime implements ValueObjectInterface
{
    /**
     * @var \DateTime
     */
    protected $dateTime;

    /**
     * @return DateTime
     */
    public static function fromNative()
    {
        return new static(@\func_get_arg(0));
    }

    /**
     * @return \DateTime
     */
    public function getRaw()
    {
        return $this->dateTime;
    }

    public function __construct($value = null)
    {
        if ($value instanceof \DateTime) {
            $this->dateTime = $value;
            return;
        }
        $this->dateTime = new \DateTime($value ?? "now");
    }

    /**
     * @return string
     */
    public function toNative()
    {
        return $this->dateTime->format("Y-m-d\TH:i:s");
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->toNative();
    }

    public function __call($name, $arguments)
    {
        return call_user_func_array([$this->dateTime, $name], $arguments);
    }
}