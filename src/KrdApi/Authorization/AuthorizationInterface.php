<?php
/**
 * Created by PhpStorm.
 * User: Pawel Plocharczyk
 * Date: 18.04.2017
 * Time: 11:00
 */

namespace KrdApi\Authorization;


interface AuthorizationInterface
{
    public function getHeader();
}