<?php
/**
 * Created by PhpStorm.
 * User: Pawel Plocharczyk
 * Date: 18.04.2017
 * Time: 11:00
 */

namespace KrdApi\Authorization;


use KrdApi\ValueObject\StringValue;

class LoginAndPasswordAuthorization implements AuthorizationInterface
{
    private $login;
    private $password;

    public function __construct(StringValue $login, StringValue $password)
    {
        $this->login = $login;
        $this->password = $password;
    }

    public function getHeader()
    {
        return [
            'Login' => $this->login->toNative(),
            'Password' => $this->password->toNative(),
            'AuthorizationType' => 'LoginAndPassword'
        ];
    }
}