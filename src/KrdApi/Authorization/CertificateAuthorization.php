<?php
/**
 * Created by PhpStorm.
 * User: Pawel Plocharczyk
 * Date: 18.04.2017
 * Time: 11:00
 */

namespace KrdApi\Authorization;


use KrdApi\ValueObject\StringValue;
use StdDomain\Entity\AccessibleEntityTrait;
/** TODO: Certyfikat auth */
class CertificateAuthorization implements AuthorizationInterface
{
    use AccessibleEntityTrait;
    private $certificate;
    private $passphrase;

    public function __construct(StringValue $certificate, StringValue $passphrase)
    {
        $this->certificate = $certificate;
        $this->passphrase = $passphrase;
    }

    public function getHeader()
    {
        return [
            'AuthorizationType' => 'Certificate'
        ];
    }
}