<?php
/**
 * Created by PhpStorm.
 * User: Pawel Plocharczyk
 * Date: 19.04.2017
 * Time: 11:36
 */
require_once('vendor/autoload.php');

$api = new \KrdApi\KrdApi(
    new \KrdApi\Authorization\LoginAndPasswordAuthorization(
        \KrdApi\ValueObject\StringValue::fromNative('login'),
        \KrdApi\ValueObject\StringValue::fromNative('password')
    )
);

$api->registerService(
    new \KrdApi\Service\SearchCustomer(
        new \KrdApi\Service\ServiceParameters(
            \KrdApi\ValueObject\StringValue::fromNative('wsdl link or ref'),
            \KrdApi\ValueObject\StringValue::fromNative('namespace')
        ),
        new \KrdApi\Source\KrdRemote()
    )
);

/** @var \KrdApi\Service\SearchCustomer $searchCustomer */
$searchCustomer = $api->getService(\KrdApi\Service\SearchCustomer::class);

$searchCustomerRequest = new \KrdApi\Request\SearchCustomerRequest(
    \KrdApi\ValueObject\NumberType::byName('Pesel'),
    \KrdApi\ValueObject\StringValue::fromNative('pesel_numb'),
    new \KrdApi\ValueObject\DateTime()
);

$searchCustomer->execute($searchCustomerRequest);